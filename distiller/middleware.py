import logging
from datetime import datetime
from django.contrib.sessions import middleware
from django.conf import settings

from django.contrib.auth.middleware import RemoteUserMiddleware
from shibboleth.middleware import ShibbolethRemoteUserMiddleware
from django.contrib.auth.models import Group
from django.contrib import auth
from django.core.exceptions import ImproperlyConfigured
from django.shortcuts import redirect

from shibboleth.app_settings import SHIB_ATTRIBUTE_MAP, LOGOUT_SESSION_KEY

from context_processors import *


logger = None


class PhonyLogger(object):

    def debug(self, *args, **kwargs):
        pass

    def info(self, *args, **kwargs):
        pass

    def warning(self, *args, **kwargs):
        pass

    def error(self, *args, **kwargs):
        pass

    def critical(self, *args, **kwargs):
        pass


class LoggingMiddleware(object):
    """
    Basic logging middleware. If settings.LOG_ENABLED is set, adds a logger
    instance as request.logger

    Logging can be used as:

        request.logger.[debug, info, warning, error and critical]

    Ex: request.logger.info("This is an info message")

    Requires LOG_ENABLED settings value.

    If settings.LOG_ENABLED is True, requires LOG_FILE value.

    LOG_NAME is optional, and will specify a name for this logger
    instance (not shown in default format string)
    """

    def process_request(self, request):
        from django.conf import settings
        enabled = getattr(settings, 'LOG_ENABLED', False)
        logfile = getattr(settings, 'LOG_FILE', None)
        if not enabled or not logfile:
            request.logger = PhonyLogger()
            return

        global logger
        if logger is None:
            logging.basicConfig(
                level=logging.DEBUG,
                format='%(asctime)s - %(levelname)s - %(message)s',
                datefmt='%Y-%m-%d %X',
                filename=settings.LOG_FILE,
                filemode='a')
            logger = logging.getLogger(getattr(settings, 'LOG_NAME', 'django'))
            logger.setLevel(logging.DEBUG)

        request.logger = logger


class MyExceptionMiddleware(object):

    def process_exception(self, request, exception):
        request.logger.info('exception detected - MyExceptionMiddleware')
        if exception is ShibbolethValidationError:
            redirect('https://voparis-cta-test.obspm.fr/accounts/login')
        else:
            raise


class ProxyRemoteUserMiddleware(RemoteUserMiddleware):
    # When using a ssl proxy, REMOTE_USER is found as HTTP_X_REMOTE_USER
    header = 'HTTP_X_REMOTE_USER'


class ShibbolethValidationError(Exception):
    pass


class ProxyShibbolethRemoteUserMiddleware(ShibbolethRemoteUserMiddleware):

    # When using a ssl proxy, REMOTE_USER is found as HTTP_X_REMOTE_USER
    header = 'HTTP_X_REMOTE_USER'

    def process_request(self, request):
        # Return if REMOTE_USER is not set or is empty
        try:
            username = request.META[self.header]
        except KeyError:
            return
        if request.META[self.header] == '':
            #print 'REMOTE_USER is empty'
            return
        # Continue with original process_request()
        request.logger.info('shibmw request.session.session_key = {}'.format(request.session.session_key))

        # AuthenticationMiddleware is required so that request.user exists.
        if not hasattr(request, 'user'):
            raise ImproperlyConfigured(
                "The Django remote user auth middleware requires the"
                " authentication middleware to be installed.  Edit your"
                " MIDDLEWARE_CLASSES setting to insert"
                " 'django.contrib.auth.middleware.AuthenticationMiddleware'"
                " before the RemoteUserMiddleware class.")

        # To support logout.  If this variable is True, do not
        # authenticate user and return now.
        if request.session.get(LOGOUT_SESSION_KEY):
            return
        else:
            # Delete the shib reauth session key if present.
            request.session.pop(LOGOUT_SESSION_KEY, None)

        # Locate the remote user header.
        try:
            username = request.META[self.header]
        except KeyError:
            # If specified header doesn't exist then return (leaving
            # request.user set to AnonymousUser by the
            # AuthenticationMiddleware).
            return
        # If the user is already authenticated and that user is the user we are
        # getting passed in the headers, then the correct user is already
        # persisted in the session and we don't need to continue.
        if request.user.is_authenticated():
            if request.user.username == self.clean_username(username, request):
                return

        # Make sure we have all required Shiboleth elements before proceeding.
        shib_meta, error = self.parse_attributes(request)
        # Add parsed attributes to the session.
        request.session['shib'] = shib_meta
        if error:
            #raise ShibbolethValidationError("All required Shibboleth elements"
            #                                " not found.  %s" % shib_meta)
            #request.logger('All required Shibboleth elements not found.  %s' % shib_meta)
            auth.logout(request)
            return redirect(settings.SHIBBOLETH_LOGOUT_URL + '?return=https://voparis-cta-test.obspm.fr/accounts/login')

        # We are seeing this user for the first time in this session, attempt
        # to authenticate the user.
        user = auth.authenticate(remote_user=username, shib_meta=shib_meta)
        if user:
            # User is valid.  Set request.user and persist user in the session
            # by logging the user in.
            request.user = user
            auth.login(request, user)

            # Upgrade user groups if configured in the settings.py
            # If activated, the user will be associated with those groups.
            #if GROUP_ATTRIBUTES:
            #    self.update_user_groups(request, user)
            # call make profile.
            self.make_profile(user, shib_meta)
            # setup session.
            self.setup_session(request)

        request.logger.info('shibmw request.session.session_key = {}'.format(request.session.session_key))

    def make_profile(self, user, shib_meta):
        user.email = shib_meta['mail']

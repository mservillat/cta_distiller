from django.db import models

# Create your models here.

# problem with oidc: need longer strings in some fields:
from django.contrib.auth.models import User
User._meta.get_field_by_name('username')[0].max_length = 200
User._meta.get_field('username').validators[0].limit_value = 200
User._meta.get_field_by_name('first_name')[0].max_length = 200
User._meta.get_field('first_name').validators[0].limit_value = 200
User._meta.get_field_by_name('last_name')[0].max_length = 200
User._meta.get_field('last_name').validators[0].limit_value = 200

# after changes do:
# python2 manage.py makemigrations
# python2 manage.py migrate


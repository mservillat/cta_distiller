from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.forms.widgets import PasswordInput, TextInput

# from crispy_forms.helper import FormHelper
# from crispy_forms.layout import Layout,Fieldset,Submit,Button,Div
# from crispy_forms.bootstrap import FormActions

# Set attributes for bootstrap style
attr_bs = {'class' : 'form-control'}


class AuthenticationFormBS(AuthenticationForm):
    username = forms.CharField(widget=TextInput(attrs=attr_bs))
    password = forms.CharField(widget=PasswordInput(attrs=attr_bs))

class ConeSearchForm(forms.Form):        
    target_name = forms.CharField(
        initial = 'PKS 2155-304',
        label = 'Target Name', 
        help_text = 'Used to query Simbad with Sesame and set RA/Dec.',
        required = False, 
        widget = forms.TextInput(attrs=attr_bs))
    s_ra = forms.FloatField(
        initial = '329.717',
        label = 'Source RA (deg)',
        help_text = 'Right Ascension.',
        required = True, 
        widget = forms.TextInput(attrs=attr_bs))
    s_dec = forms.FloatField(
        initial = '-30.226',
        label = 'Source Dec (deg)', 
        help_text = 'Declination.',
        required = True, 
        widget = forms.TextInput(attrs=attr_bs))
    search_rad = forms.FloatField(
        initial = '0.001', 
        label = 'Search radius (deg)', 
        help_text = '',
        required = True, 
        widget = forms.TextInput(attrs=attr_bs))

class ObsCoreSearchForm(forms.Form):        
    proposal_id = forms.CharField(
        label = 'proposal_id', 
        help_text = 'Proposal ID',
        required = False, 
        widget = forms.TextInput(attrs=attr_bs))
    dataproduct_type_choices = (
        ('', ''),
        ('eventlist', 'Event Lists'),
        ('image', 'Images'),
        ('spectrum', 'Spectra'),
        ('timeseries', 'Time Series')
    )
    dataproduct_type = forms.ChoiceField(
        label = 'dataproduct_type', 
        choices = dataproduct_type_choices, 
        help_text = 'Data product (file content) primary type',
        required = False, 
        widget = forms.Select(attrs=attr_bs))
    dataproduct_level_choices = (
        ('', ''),
        ('DL0', 'DL0'), 
        ('DL1', 'DL1'), 
        ('DL2', 'DL2'), 
        ('DL3', 'DL3'), 
        ('DL4', 'DL4'), 
        ('DL5', 'DL5'), 
    )
    dataproduct_level = forms.ChoiceField( # ObsCoreCTA
        label = 'dataproduct_level', 
        choices = dataproduct_level_choices, 
        help_text = 'DL0-5',
        required = False, 
        widget = forms.Select(attrs=attr_bs))
    obs_id = forms.FloatField(
        label = 'obs_id', 
        help_text = 'Run ID',
        required = False, 
        widget = forms.TextInput(attrs=attr_bs))
    t_min = forms.FloatField(
        label = 't_min', 
        help_text = 'Start time in MJD',
        required = False, 
        widget = forms.TextInput(attrs=attr_bs))
    t_max = forms.FloatField(
        label = 't_max', 
        help_text = 'Stop time in MJD',
        required = False, 
        widget = forms.TextInput(attrs=attr_bs))
#     t_exptime = forms.ChoiceField(
#         label = 't_exptime', 
#         choices = dataproduct_type_choices, 
#         help_text = 'Total exposure time',
#         required = False, 
#         widget = forms.Select(attrs=attr_bs))
    em_min_tev = forms.FloatField( # ObsCoreCTA
        label = 'em_min_tev', 
        help_text = 'Start in spectral coordinates',
        required = False, 
        widget = forms.TextInput(attrs=attr_bs))
    em_max_tev = forms.FloatField( # ObsCoreCTA
        label = 'em_max_tev', 
        help_text = 'Stop in spectral coordinates',
        required = False, 
        widget = forms.TextInput(attrs=attr_bs))
    
class JobParameterForm(forms.Form):        
    inobs = forms.CharField(
        initial = '', #'http://voplus.obspm.fr/cta/events.fits', 
        label = 'Inout eventlist file',
        help_text = 'URL to access the eventlist file',
        required = True, 
        widget = forms.TextInput(attrs=attr_bs))
    enumbins = forms.IntegerField(
        initial = '1', 
        label = 'Number of energy bins', 
        required = True, 
        widget = forms.NumberInput(attrs=attr_bs))

def make_ParameterForm(params, data={}, initial={}):
    # Create base fields for ParameterForm
    import collections
    fields = collections.OrderedDict()
    for p in params:
        field = forms.CharField
        widget = forms.TextInput(attrs=attr_bs)
        if p['name'] in initial.keys():
            p_initial = initial[p['name']]
        else:
            p_initial = p['default']
        kwargs = {
            'initial': p_initial,
            'label': p['name'], 
            'help_text': p['prompt'],
            'required': True, 
            'widget': widget
        }
        fields[p['name']] = field(**kwargs)
    # Create attribut dict for type()
    attr_dict = {'base_fields': fields}
    if data != {}:
        attr_dict['data'] = data
        print 'data added'
    form = type("ParameterForm", (forms.BaseForm,),  attr_dict)
    return form

class ParameterForm(forms.Form):
    def __init__(self, *args, **kwargs):
        form_params = kwargs.pop("params", {})
        super(ParameterForm, self).__init__(*args, **kwargs)
        for p in form_params:
            if p['required'] == 'true':
                field = forms.CharField
                widget = forms.TextInput(attrs=attr_bs)
                field_kwargs = {
                    'initial': p['default'],
                    'label': p['name'], 
                    'help_text': p['prompt'],
                    'required': True, 
                    'widget': widget
                }
                self.fields[p['name']] = field(**field_kwargs)



# additional function called directly from templates
# Add the followinf context processor to settings.py:
# TEMPLATE_CONTEXT_PROCESSORS = (
#    'app.context_processors.app_url',
# )

from django import template
from django.conf import settings

register = template.Library()

# The app can be accessed from https://voparis-auth.obspm.fr/cta-client
# It is then needed to rewrite the urls given in templates with a prefix
# given with this function
def app_url(request):
    host = request.get_host()
    app_url = ''
    if (host == 'voparis-auth.obspm.fr'):
        app_url = 'https://voparis-auth.obspm.fr/cta-client' # '/cta-client' #
    return { 'app_url': app_url }

# @register.simple_tag
# def settings_value(name):
#     from django.conf import settings
#     return getattr(settings, name, "")

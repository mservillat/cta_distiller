import os
import uuid
import base64
import tempfile
import zipfile
import logging
import urllib, urllib2
import requests
import json
from django.shortcuts import render_to_response, redirect, render
from django.http import HttpResponse, Http404, HttpResponseNotFound, HttpResponseRedirect, HttpResponseServerError, StreamingHttpResponse
from django.views.decorators.http import require_safe, require_POST, require_GET
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.core.servers.basehttp import FileWrapper
from django.utils.html import escape
#from revproxy.views import ProxyView
import views_helper
from context_processors import *
from forms import *
from django.conf import settings

# Get an instance of a logger
logger = logging.getLogger(__name__)
js_logger = logging.getLogger('javascript')


# Global variables
ALL_REC = 1000
MAX_REC = 1000000


#----------
# HOME


@require_safe
def home(request, **kwargs):
    logger.info('Start Home')
    """
    Welcome page, explains the project
    """
    request.session['page_title'] = 'Home'
    return render(request, 'distiller/home.html')


#----------
# LOGGING from javascript


@require_POST
def js_log(request, **kwargs):
    """
    Log from javascript code
    """
    lvl = int(request.POST['lvl'])
    msg = request.POST['msg']
    if lvl >= 40:
        msg += '\n  exception: '+request.POST['exception']
        msg += '\n  context: '+request.POST['context']
        if request.user.is_authenticated():
            msg += '\n  user: '+request.user.username
        msg += '\n  stack:\n    '+request.POST['stack']
    js_logger.log(lvl, msg)
    return HttpResponse('Error logged')


@require_safe
@login_required
@user_passes_test(lambda u: u.is_superuser)
def show_logs(request, **kwargs):
    """
    display logs
    """
    request.session['page_title'] = 'Logs'
    from settings import LOGGING
    log_name = LOGGING['handlers']['file_app']['filename']
    f = open(log_name,'r')
    app_debug = f.readlines()
    app_debug.reverse()
    app_debug = ''.join(app_debug)
    f.close()
    return render(request, 'distiller/show_logs.html', {'text': app_debug})


@require_safe
def show_info(request, **kwargs):
    """
    display info on server
    """
    request.session['page_title'] = 'Info'
    dict_list = [u'***** DJANGO SESSION *****']
    for key, val in request.session.iteritems():
        if key == 'shib':
            for key2, val2 in val.iteritems():
                try:
                    dict_list.append(u'shib: ' + key2 + u' = ' + val2)
                except:
                    dict_list.append(u'shib: ' + key2 + u' *** UnicodeEncodeError ***')
        else:
            dict_list.append(key + u' = ' + unicode(val))
    dict_list.append(u'***** USER *****')
    if request.user.is_authenticated():
        dict_list.append(u'username = '+request.user.get_username())
        dict_list.append(u'full_name = '+request.user.get_full_name())
        dict_list.append(u'email = '+request.user.email)
        dict_list.append(u'last_login = '+unicode(request.user.last_login))
        dict_list.append(u'permissions = '+unicode(request.user.get_all_permissions()))
    else:
        dict_list.append(u'No user authenticated')
    dict_list.append(u'***** REQUEST META *****')
    dict_list_meta = []
    for key, val in request.META.iteritems():
        try:
            dict_list_meta.append(unicode(key) + u' = ' + val)
        except:
            dict_list_meta.append(unicode(key) + u' *** ERROR ***w')
    dict_list_meta.sort()
    dict_str = u'\n'.join(dict_list) + u'\n' + u'\n'.join(dict_list_meta)
    return render(request, 'distiller/show_logs.html', {'text': dict_str})

def show_request(request):
    """
    display request
    """
    request_str = escape(repr(request))
    logger.debug(request_str)
    return HttpResponse(request_str)

#----------
# LOGIN


def login_view(request, **kwargs):
    """
    Ask user for name and password, and authenticate
    """
    request.session['page_title'] = 'Login'
    username = password = ''
    next_url = '/home'  # '/show_info'  #
    if 'next' in request.GET:
        next_url = request.GET['next']

    # request.session.pop(LOGOUT_SESSION_KEY, None)
    # if user.is_active:
    # TODO: Test if user is authenticated through Shibboleth

    if request.POST:
        # AuthenticationForm has been posted to the view for local authentication
        form = AuthenticationFormBS(request.POST)
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                # request.session.flush()
                login(request, user)
                logger.info('user logged in: '+request.user.get_username())
                try:
                    if not user.get_full_name():
                        user.first_name = user.username
                except:
                    logger.info('user logged in: '+request.user.get_username())
                # Redirect to a success page.
                return redirect(request.POST['next'])
            else:
                # Return a 'disabled account' error message
                logger.warning('login failed')
                context = {'form': form,
                           'next': next_url,
                           'shib_login_url': settings.SHIBBOLETH_LOGIN_URL,
                           'shib_app_url': settings.SHIBBOLETH_APP_URL,
                           }
                return render(request, 'distiller/login_django.html', context)
        else:
            # Return an 'invalid login' error message.
            logger.warning('user is None')
            context = {'form': form,
                       'next': next_url,
                       'shib_login_url': settings.SHIBBOLETH_LOGIN_URL,
                       'shib_app_url': settings.SHIBBOLETH_APP_URL,
                       }
            return render(request, 'distiller/login_django.html', context)
    else:
        logger.info('user login form')
        form = AuthenticationFormBS()
        context = {'form': form,
                   'next': next_url,
                   'shib_login_url': settings.SHIBBOLETH_LOGIN_URL,
                   'shib_app_url': settings.SHIBBOLETH_APP_URL,
                   }
        return render(request, 'distiller/login_django.html', context)


@require_safe
def logout_view(request, **kwargs):
    """
    Logout user
    """
    logger.info('user logged out: '+request.user.get_username())
    if 'shibboleth' in request.session['_auth_user_backend']:
        logger.info('Logout Shibboleth')
        # request.session[LOGOUT_SESSION_KEY] = True
        logout(request)
        return redirect(settings.SHIBBOLETH_LOGOUT_URL + '?return=' + app_url(request)['app_url'] + '/home')
    elif 'social.backends' in request.session['_auth_user_backend']:
        backend_name = request.session['social_auth_last_login_backend']
        #r = requests.post('http://' + request.get_host() + '/disconnect/' + backend_name)
        #print r
    logout(request)
    return redirect(app_url(request)['app_url'] + '/home')


@require_GET
@login_required
@user_passes_test(lambda u: u.is_superuser)
def create_user(request, **kwargs):
    """
    Create user from GET request, e.g.:
    create_user?username=user&email=mathieu.servillat@obspm.fr&password=cta
    """
    if request.user.is_authenticated() and request.user.is_superuser:
        if request.GET:
            from django.contrib.auth.models import User
            try:
                user = User.objects.create_user(request.GET['username'], request.GET['email'], request.GET['password'])
                logger.info('User ' + request.GET['username'] + ' created')
            except Exception as ex:
                logger.error(ex)
                print 'Exception:', ex
    else:
        logger.warning('Non admin tried to create a new user')
        print 'Need to be admin to create a new user'
    return redirect(app_url(request)['app_url'] + '/home')


#----------
# SEARCH FORM


def search_form(request, **kwargs):
    """
    Show main search form and generate the ADQL query for the TAP server
    """
    request.session['page_title'] = 'Search Form'
    if request.GET:
        # The form was filled, let's process the inputs
        form_cone = ConeSearchForm(request.GET, error_class=views_helper.DivErrorList)
        form_obscore = ObsCoreSearchForm(request.GET, error_class=views_helper.DivErrorList)
        query_where = ''
        # Asking for a Cone Search?
        if 'do_cone_search' in request.GET:
            if form_cone.is_valid() and form_obscore.is_valid():
                # Prepare ADQL query
                fcd = form_cone.cleaned_data
                query_vars = map(str,(fcd['s_ra'],fcd['s_dec'],fcd['search_rad']))
                query_where += '1 = intersects(o.s_region, circle(\'ICRS\', %f, %f, %f))' \
                               '' % (fcd['s_ra'],fcd['s_dec'],fcd['search_rad'])  # tuple(query_vars)
                # query_where = '1 = intersects(circle(\'ICRS\', s_ra, s_dec, s_fov), circle(\'ICRS\', %s, %s, %s))' \
                #               '' % tuple(query_vars)
            else:
                # The form is not valid, send error
                logger.debug('errors in submitted form')
                context = {'form_cone': form_cone, 'form_obscore': form_obscore}
                return render(request, 'distiller/search_form.html', context)
        # Process ObsCOre Search 
        if form_obscore.is_valid():
            fcd = form_obscore.cleaned_data
            for key, val in fcd.iteritems():
                if val:
                    logger.debug(key + ' = ' + str(val))
                else:
                    logger.debug(key + ' = NOT DEFINED')
        else:
            # The form is not valid, send error
            logger.debug('errors in submitted form')
            context = {'form_cone': form_cone, 'form_obscore': form_obscore}
            return render(request, 'distiller/search_form.html', context)
            
        select_query = 'SELECT * FROM hess_dr.vo_obscore as o'
        if query_where:
            select_query += ' WHERE ' + query_where
        # Keep query and url in the session
        request.session['ADQL_query'] = select_query
        request.session['ADQL_url'] = views_helper.query2url(select_query, settings.URL_TAP)
        logger.debug(request.session['ADQL_url'])
        # The ADQL query will be sent via an ajax command on the next webpage, using the query_tap function below
        logger.debug('valid form submitted')
        return redirect(app_url(request)['app_url'] + '/results')
    else:
        # Show form to be filled
        form_cone = ConeSearchForm(error_class=views_helper.DivErrorList)
        form_obscore = ObsCoreSearchForm(request.POST, error_class=views_helper.DivErrorList)
        context = {'form_cone': form_cone, 'form_obscore': form_obscore}
        return render(request, 'distiller/search_form.html', context)


@require_GET
def query_sesame(request):
    """
    Query Simbad using Sesame, return ra/dec/names in json
    """
    if 'src_name' in request.GET:
        # Read result of Sesame query
        src_name = request.GET['src_name'].strip()
        url = settings.URL_SESAME + '?' + urllib.urlencode({src_name: ''})[:-1]
        # request.GET['src_name'].strip()
        print url
        web_file = urllib2.urlopen(url)
        s = web_file.read()
        web_file.close()
        #print s
        # Parse the result to extract info on source (name, type, RA, Dec)
        result = views_helper.parse_sesame_result(s)
        json_data = json.dumps(result)
        if result['found']:
            # Return info as json data
            logger.info('Found source with Sesame: '+src_name)
            return HttpResponse(json_data, content_type="application/json")
        else:
            # No results found by Sesame
            logger.info('Source not found with Sesame: '+src_name)
            return HttpResponse(json_data, content_type="application/json")
    else:
        # Wrong call to query_sesame(), requires src_name
        logger.warning('Wrong call to query_sesame(), requires src_name in request.GET')
        return HttpResponse(status=404)


#----------
# SHOW RESULTS


@require_safe
def show_results(request, **kwargs):
    """
    Show last results from ADQL query stored in session
    """
    logger.debug('Cookies: ' + str(request.COOKIES))
    logger.debug(settings.SESSION_COOKIE_DOMAIN)
    logger.debug(settings.SESSION_COOKIE_PATH)
    request.session['page_title'] = 'Results'
    if 'ADQL_query' in request.GET.keys():
        query = request.GET['ADQL_query']
        request.session['ADQL_query'] = query
        request.session['ADQL_url'] = views_helper.query2url(query, settings.URL_TAP)
    if 'ADQL_url' in request.session:
        logger.info('showing results for ADQL_query='+request.session['ADQL_query'])
        logger.info('showing results for ADQL_url='+request.session['ADQL_url'])
        return render(request, 'distiller/show_results.html')
    else:
        logger.warning('Cannot show results, ADQL_url is not set')
        return redirect(app_url(request)['app_url'] + '/search')


@require_safe
def query_tapserver(request, **kwargs):
    """
    Query DaCHs TAP server with an ADQL query and return a json table
    """
    # Read result of ADQL_url 
    try:
        url = request.session["ADQL_url"]
        req = urllib2.Request(url)
        web_file = urllib2.urlopen(req)
        votable = web_file.read()
        web_file.close()
#         local_file = open('test_table.vo', 'w')
#         local_file.writelines(votable)
#         local_file.close()
        logger.info('data received from TAP server')
        # The obtained VO Table has to be converted to json for dataTable
        logger.info('before vo2json')
        table_data = views_helper.vo2json(votable)
        logger.debug('vo2json done')
        # table_data_json = {}
        json.encoder.FLOAT_REPR = lambda o: format(o, '.2f')
        logger.info('before json.dumps')
        table_data_json = json.dumps(table_data)
        logger.debug('json.dumps done')
        return HttpResponse(table_data_json, content_type="application/json")    
    except Exception as ex:
        logger.error(ex)
        return StreamingHttpResponse(ex, status=500)


@require_GET
#@login_required
def get_data(request):
    """
    Send a data file
    """
    request.session['page_title'] = ''
    PROJECT_PATH = os.path.realpath(os.path.dirname(__file__))
    data_name = request.GET['name']
    data_name = data_name.replace('..', '')
    filename = PROJECT_PATH + '/../data/' + data_name
    if os.path.isfile(filename):
        logger.info('File found and returned: ' + filename)
        wrapper = FileWrapper(file(filename))
        response = StreamingHttpResponse(wrapper, content_type='text/plain')
        response['Content-Length'] = os.path.getsize(filename)
        return response
    else:
        logger.warning('File not found: ' + filename)
        return HttpResponse(status=404)


#----------
# SEND FILE


# def send_file(request):
#     """                                                                         
#     Send a file through Django without loading the whole file into              
#     memory at once. The FileWrapper will turn the file object into an           
#     iterator for chunks of 8KB. 
#     https://djangosnippets.org/snippets/365/                                                
#     """
#     filename = __file__ # Select your file here.                                
#     wrapper = FileWrapper(file(filename))
#     response = HttpResponse(wrapper, content_type='text/plain')
#     response['Content-Length'] = os.path.getsize(filename)
#     return response
# 
# def send_zipfile(request):
#     """                                                                         
#     Create a ZIP file on disk and transmit it in chunks of 8KB,                 
#     without loading the whole file into memory. A similar approach can          
#     be used for large dynamic PDF files. 
#     https://djangosnippets.org/snippets/365/                                       
#     """
#     temp = tempfile.TemporaryFile()
#     archive = zipfile.ZipFile(temp, 'w', zipfile.ZIP_DEFLATED)
#     for index in range(10):
#         filename = __file__ # Select your files here.                           
#         archive.write(filename, 'file%d.txt' % index)
#     archive.close()
#     wrapper = FileWrapper(temp)
#     response = HttpResponse(wrapper, content_type='application/zip')
#     response['Content-Disposition'] = 'attachment; filename=test.zip'
#     response['Content-Length'] = temp.tell()
#     temp.seek(0)
#     return response


#----------
# MANAGE JOBS


# class UwsProxyView(ProxyView):
#     upstream = 'http://voparis-uws.obspm.fr/uws-v1.0/'
#     add_remote_user = True


@require_safe
@login_required
def job_list(request, **kwargs):
    """
    Show job list
    """
    request.session['page_title'] = 'Job List'
    pid = uuid.uuid5(uuid.NAMESPACE_X500, settings.BASE_DIR)
    request.session['auth'] = base64.b64encode(request.user.username + ':' + str(pid))

    # If current job has been deleted, remove the session variable
    # Remove job_id from session if job_delete in GET
    if 'job_deleted' in request.GET or 'job_missing' in request.GET:
        if 'job_id' in request.session:
            del request.session['job_id']
    logger.info('showing job list')
    return render(request, 'distiller/job_list.html')


@require_safe
@login_required
def job_edit(request, job_name, job_id, **kwargs):
    """
    Show job description, parameters and results
    """
    request.session['page_title'] = 'Selected Job'
    pid = uuid.uuid5(uuid.NAMESPACE_X500, settings.BASE_DIR)
    request.session['auth'] = base64.b64encode(request.user.username + ':' + str(pid))
    params = views_helper.read_wadl(job_name)
    form = ParameterForm(params=params, error_class=views_helper.DivErrorList)
    if job_name and job_id:
        request.session['job_name'] = job_name
        request.session['job_id'] = job_id
        logger.info('showing selected ' + job_name + 'job: '+job_id)
        context = {'form': form}
        return render(request, 'distiller/job_edit.html', context)
    else:
        return redirect(app_url(request)['app_url'] + '/job_list')

@require_safe
@login_required
def job_form(request, job_name, **kwargs):
    """
    Show form to create a job
    """
    request.session['page_title'] = 'Create Job'
    pid = uuid.uuid5(uuid.NAMESPACE_X500, settings.BASE_DIR)
    request.session['auth'] = base64.b64encode(request.user.username + ':' + str(pid))
    if 'evfile' in request.GET:
        # Prepare form with initial values from GET
        # 1/ the dataset access_url is given from show_results
        # 2/ the full form has been filled, the uws_client should start the job, then move to job_edit
        data = request.GET
        #form = JobParameterForm(initial=data, error_class=views_helper.DivErrorList)
        params = views_helper.read_wadl(job_name)
        form = ParameterForm(params=params, initial=data, error_class=views_helper.DivErrorList)
    else:
        #form = JobParameterForm(error_class=views_helper.DivErrorList)
        params = views_helper.read_wadl(job_name)
        form = ParameterForm(params=params, error_class=views_helper.DivErrorList)
    # Show form to be filled
    context = {'form': form, 'job_name': job_name}
    return render(request, 'distiller/job_form.html', context)


@require_safe
@login_required
def job_send(request, job_name, **kwargs):
    """
    Send job parameters to UWS server for creation
    """
    if request.GET:
        # Form has been submitted
        params = views_helper.read_wadl(job_name)
        form = ParameterForm(request.GET, params=params, error_class=views_helper.DivErrorList)
        #form = JobParameterForm(request.GET, error_class=views_helper.DivErrorList)
        if form.is_valid():
            # Process form
            logger.info('form is valid, create new job')
            context = {'form': form, 'job_name': job_name, 'label_size': 2, 'input_size': 5, 'help_size': 5 }
            return render(request, 'distiller/inc_form.html', context)
        else:
            # Error in form
            logger.warning('form has errors')
            context = {'form': form, 'job_name': job_name, 'label_size': 2, 'input_size': 5, 'help_size': 5 }
            return render(request, 'distiller/inc_form.html', context)
    else:
        return redirect(app_url(request)['app_url']+'/job_form')


@require_safe
@login_required
def update_job_id(request, **kwargs):
    if 'job_id' in request.GET:
        request.session['job_id'] = request.GET['job_id']
    return HttpResponse('OK')


#----------
# VISUALISATION


@login_required
def visu_js9(request, **kwargs):
    """
    View FITS file with js9
    """
    request.session['page_title'] = 'View with JS9'
    if 'fits_url' in request.GET:
        request.session['fits_url'] = request.GET['fits_url']
    if 'fits_url' in request.POST:
        request.session['fits_url'] = request.POST['fits_url']
    logger.info('viewing ' + request.session['fits_url'])
    return render(request, 'distiller/visu_js9.html')




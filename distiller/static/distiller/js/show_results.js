( function($) {
	"use strict";
	
	$(document).ready( function() {

		//var new_query = $("#ADQL_query").attr('value').replace('cta.', 'hess_dr.');
		// $("#ADQL_query").attr('value', new_query);
		//var new_url = $("#ADQL_url").attr('value');
		//new_url = new_url.replace('cta.', 'hess_dr.');
		//$("#ADQL_url").attr('value', new_url);
		// $("#variables").append('<div id="ADQL_query_var" style="display:none" value="' + new_query + '"></div>')

		// Show loading animated image
		$("#div_loading").show();

		//var myCodeMirror = CodeMirror.fromTextArea( $('#ADQL_query') );

		// Select/deselect buttons if a row is selected or not
		var toggle_buttons = function() {
			//var oTT = TableTools.fnGetInstance('results_table');
			//var aData = oTT.fnGetSelectedData();
			var numberOfChecked = $('td.bs-checkbox input:checkbox:checked').length;
			console.log(numberOfChecked, $('td.bs-checkbox input:checkbox:checked'));
			if (numberOfChecked == 0) {
				$('#samp_data_selection').attr("disabled", "disabled");
				$('#job_ctbin').attr("disabled", "disabled");
				$('#job_sp').attr("disabled", "disabled");
			} else {
				$('#samp_data_selection').removeAttr("disabled");
				$('#job_ctbin').removeAttr("disabled");
				//$('#job_sp').removeAttr("disabled");
			}
		}; // end toggle_buttons

		// Send query to TAP server and show table
		var query_tapserver = function(table) {
			$.ajax({
				url: $("#query_tapserver").attr('value'),
				type: 'GET',
				data: { ADQL_url: $("#ADQL_url").attr('value') },
				dataType: 'json',
				error: function( jqXHR, textStatus, errorThrown ) {
					$("#div_loading").hide();
					$("#div_error").html(errorThrown+": "+textStatus);
					$("#div_error").show();
				},
				success: function( data, textStatus, jqrXHR ) {
					//console.log(JSON.stringify(data['data']));
					query_tapserver_success_bs(table, data);
				}
			}); // end ajax
		};

		// Create Boostrap table with the results
		var query_tapserver_success_bs = function(table, data) {
            table.bootstrapTable({
                data: data['data'],
                columns: data['columns'],
                selectItemName: 'state',
                idField: 'id',
                classes: 'table table-hover',
                //striped: true,
                showColumns: true,
                minimumCountColumns: 0,
                pagination: true,
                pageSize: 5,
                pageList: [5, 10, 25, 50, 100, 200],
                clickToSelect: true,
                singleSelect: true,
                maintainSelected: true,
                //showRefresh: true,
                showToggle: true,
                search: true,
                showExport: true,
                exportTypes: ['pdf', 'csv', 'txt', 'json', 'xml', 'sql', 'excel'],
                showFilters: true,
                toolbar: "#filter-bar"
            });
            $('#filter-bar').bootstrapTableFilter({
                filters: data['filters'],
                connectTo: '#results_table'
//                onSubmit: function() {
//                    var data = $('#filter-bar').bootstrapTableFilter('getData');
//                    console.log(data);
//                }
            });
			//$("#div_table div.search").removeClass("pull-right").addClass("pull-left");
			$("#div_loading").hide();
			$("#div_table").show();
			// Disable buttons as no row selected
			$('#samp_data_selection').attr("disabled", "disabled");
			$('#job_ctbin').attr("disabled", "disabled");
			$('#job_sp').attr("disabled", "disabled");
			// toggle_buttons on click
			$("td.bs-checkbox input:checkbox").change( function() {
				toggle_buttons();
			});
//			table.on('all.bs.table', function (e, name, args) {
//                console.log('Event:', name, ', data:', args, ' e=', e);
//			});
			// Recreate toggle_buttons event on column_switch (why is this needed?...)
			table.on('column-switch.bs.table', function (e) {
                console.log('bs.table event:', name);
				$("td.bs-checkbox input:checkbox").change( function() {
					toggle_buttons();
				});
            });
			$('div.pagination').click( function() {
				console.log('ul.pagination');
				$("td.bs-checkbox input:checkbox").change( function() {
					toggle_buttons();
				});
			})
		};
		
		// Put the data in a dataTable object
		var query_tapserver_success = function(table, data) {
			// Create dataTable object using data
			var datatable = table.dataTable(
				$.extend(
					{
					"bAutoWidth": true,
					"sScrollX": "99.6%", 
//						"sScrollY": "42em",
//						"bScrollCollapse": true,
					"bProcessing": true,
					"sDom": 'frCTtpil', //'<"H"lfr>t<"F"ip>', //'<"top"i>rt<"bottom"flp><"clear">', //'TRC<"clear">lfrtip', //
					"oTableTools": {
						"sRowSelect": "single",
						"aButtons": [
//						    "select_all", 
//						    "select_none",  
						    {"sExtends": "csv", "sFileName": "CTADaDi_Results.csv"} ], 
//						    {"sExtends": "print", "sInfo": "Please press escape when done", "sMessage": "CTA DADi Results"} ],
						"sSwfPath": "/media/swf/copy_csv_xls_pdf.swf" },
					"oColVis": {
						"aiExclude": [ 0 ],
						"sAlign": "left",
//						"buttonText": "&nbsp;",
//						"activate": "mouseover",
						"bRestore": true },
					"sPaginationType": "full_numbers",
					"iDisplayLength": 5,
					"aLengthMenu": [[5,10, 20, 50, 100], [5,10, 20, 50, 100]],
					"oColReorder": {} 
					},
					data
				) // end extend
			); // end dataTable
			$("#div_loading").hide();
			$("#div_table").show();
			// Disable buttons as no row selected
			$('#samp_data_selection').attr("disabled", "disabled");
			$('#job_ctbin').attr("disabled", "disabled");
			$('#job_sp').attr("disabled", "disabled");
		}; // end success
		
		// Load table into results_table
		$("#results_table").each( function() {
			var table = $(this);
			// Query TAP server with ADQL_url
			query_tapserver(table);
		}); // end each
		
		// Event click on query
		$("#div_query div span").click( function() {
			$('#form_query textarea').attr('readonly','readonly');
    		$('#form_query').submit();
		});

		// SAMP

		// Event click on SAMP button for result list
		$("#samp_all").click( function() {
			var url = $('#ADQL_url').attr('value');
			var schema = 'CTA DaDi Results';
			samp_client.samp_votable(url, schema);
			logger('INFO', 'Table sent to SAMP hub');
		});
		
		// Event click on SAMP button for dataset
		$("#samp_data_selection").click( function() {
			//var oTT = TableTools.fnGetInstance('results_table');
			//var aData = oTT.fnGetSelectedData();
			var data = $('#results_table').bootstrapTable('getSelections');
			//console.log(data);
			var i, data_list = [];
			if (data.length != 0) {
				for ( i = 0; i < data.length; i++) {
					var url = data[i]['access_url'];
					//alert(url);
					//var debug_msg = $('#debug').html();
					//$('#debug').html(debug_msg + ' ' + url);
					var data_item = {};
					data_item['access_url'] = url; //
					data_item['type'] = 'fitstable';
					data_list.push(data_item);
					logger('INFO', 'Data sent to SAMP hub: '+url);
				}
				samp_client.samp_data(data_list);
				//oTT.fnSelectNone();
			}
		});
		
		// Analysis tools

		// Event click on ctbin button
		$("#job_ctbin").click( function() {
			//var oTT = TableTools.fnGetInstance('results_table');
			//var aData = oTT.fnGetSelectedData();
			var data = $('#results_table').bootstrapTable('getSelections');
			if (data.length != 0) {
				var app_url = $("#app_url").attr('value');
		    	var url = app_url+"/job_form/ctbin";
				url = addParam(url, 'evfile', data[0]['access_url']);
				url = addParam(url, 'xref', data[0]['s_ra']);
				url = addParam(url, 'yref', data[0]['s_dec']);
				window.location.href = url;
			}
		});
		
	}); // end ready
})(jQuery);
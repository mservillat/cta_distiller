( function($) {
	"use strict";		
	$(document).ready( function() {
		$('select').removeClass('form-control');
		$('select').attr('data-width', '100%');
		$('select').selectpicker();
		// When characters are entered in src_name input, query simbad
		$('#id_src_name').autocomplete({
	        minLength: 1,
	        delay: 500,
	        //position: {my: "top left", at: "top right"},
			source: function( request, response ) {
	        	// Run query through proxy to retrieve RA/Dec
				$.ajax({
					url: $("#query_sesame").attr('value'),
					type: "GET",
					dataType: "json",
					data: 'src_name=' + encodeURIComponent(request.term),
					success: function( data, textStatus, jqXHR ) {
						// Update internal div fields ra/dec
						$('#sesame_ra').attr('value', data['ra']);
						$('#sesame_dec').attr('value', data['dec']);
						// Return the list of names/aliases in a dropdown menu
						response( $.map( data['names'], function(item) {
							var otype = ' ('+data['otype']+')';
							if (otype == ' ()') { otype = ''; }
							return { label: item+otype, value: item };  
						})); // end response
					} // end success
				}); //ajax
	        }, // end source      
			select: function( event, ui ) {
	        	// Update form ra/dec using internal ra/dec
				$('#id_src_ra').val($("#sesame_ra").attr('value'));
				$('#id_src_dec').val($("#sesame_dec").attr('value'));						
			} // end select
		}); // end autocomplete
	}); // end ready
})(jQuery);
(function($) {
	"use strict";

    $(document).ready( function() {
    	var fits_url = $('#fits_url').attr('value');
    	JS9.Load(fits_url, {scale:"log", colormap:"heat"});
    	JS9.SetZoom(4);
    });
    
})(jQuery);


//----------
// Global variables

var DEBUG = true
var app_url = $("#app_url").attr('value');

//----------
// Global functions

// Toggle visibility of a block with given ID
function toggler(ID) {
    $("#" + ID).toggle();
}

// Scroll to an anchor in the page with slow animation
function scrollToAnchor(aid){
    var elt = $("#"+ aid);
    if (elt.length != 0) {
    	$('html,body').animate( {scrollTop: elt.offset().top}, 'slow' );
    }
}

// Add a parameter to a GET request
function addParam(url, param, value) {
    var a = document.createElement('a'), regex = /[?&]([^=]+)=([^&]*)/g;
    var params = {}, match, str = []; a.href = url; value=value||"";
    while (match = regex.exec(a.search))
        if (param != match[1]) str.push(match[1] + "=" + match[2]);
    str.push(encodeURIComponent(param) + "=" + encodeURIComponent(value));
    a.search = (a.search.substring(0,1) == "?" ? "" : "?") + str.join("&");
    return a.href;
}

// Get Cookie (e.g. to allow POST request in sendLog)
function getCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

//----------
// Logger for JavaScript (sent to a Django view /js_logger)

// Send log to Django (POST on /js_logger)
function sendLog(data) {
	var xhr;
	try {
		xhr = new ActiveXObject('Msxml2.XMLHTTP');
	} catch (e1) {
		try {
			xhr = new ActiveXObject('Microsoft.XMLHTTP');
		} catch (e2) {
			xhr = new XMLHttpRequest();
		}
	}
	//xhr.open("POST", "/js_error_hook/", false);
	xhr.open("POST", app_url+"/js_logger/", false);
	xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	var cookie = getCookie('csrftoken');
	if (cookie) { xhr.setRequestHeader("X-CSRFToken", cookie); };
	var query = [];
	for (var key in data) {
		query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
	}
	xhr.send(query.join('&'));
}

// LOGGER (show info in console, and send to Django)
function logger(lvl_name, msg, exception) {
	// exception is optional
	exception = typeof exception !== 'undefined' ? exception : '';
	// Create stack from here
	var stack_all = (new Error).stack.split(/\n\s*/g);
	if (stack_all[0] == 'Error') { stack_all.splice(0,1); } // remove first line for Chrome stacks
	stack_all.splice(0,1); // remove first line of stack, i.e. this logger function
	var current_function = stack_all[0].trim();
	var stack = stack_all.join('\n    ');
	// levels as defined for python.logging
    var levels = {'CRITICAL':50, 'ERROR':40, 'WARNING':30, 'INFO':20, 'DEBUG':10, 'NOTSET':0};
    var lvl = levels[lvl_name];
	var data = {
		lvl: lvl,
		msg: msg
	}    		
    // Show short msg on the webpage in block with id='logger'
    if (10 < lvl) {
    	var lvl_class = '';
        switch (lvl_name) {
			case 'DEBUG':
			case 'INFO':
				lvl_class += 'info';
				break
    		case 'WARNING':
				lvl_class += 'warning';
    			break;
    		case 'ERROR':
    		case 'CRITICAL':
				lvl_class += 'danger';
		}	
        $('#logger')
        	.removeClass( function (index, css) {
        		return (css.match(/(^|\s)(text)\S+/g) || []).join(' ');
        	})
        	.addClass('text-'+lvl_class)
        	.html(msg);
    }
    if (lvl > 30) {
    	// Add full details on error/critical
    	data['exception'] = exception;
    	data['context'] = navigator.userAgent;    	
    	data['stack'] = stack;
    } else {
    	// Add current_function on other looger
    	data['msg'] += ' ('+current_function+')';
    }
    if (DEBUG) {
    	// Show all (also debug) in console
    	switch (lvl_name) {
			case 'DEBUG':
    			console.debug(data['msg']);
    			break;
    		case 'INFO':
    			console.info(data['msg']);
    			break;
    		case 'WARNING':
    			console.warn(data['msg']);
    			break;
    		case 'ERROR':
    		case 'CRITICAL':
    			console.error(data['msg']);
    	}
    }	
	// Send log to Django
	sendLog(data);
}

// On Error, send info to Django using the logger
window.onerror = function(msg, url, line_number, column) {
    //transform errors
    if (typeof(msg) === 'object' && msg.srcElement && msg.target) {
        if(msg.srcElement == '[object HTMLScriptElement]' && msg.target == '[object HTMLScriptElement]'){
            msg = 'Error loading script';
        }else{
            msg = 'Event Error - target:' + msg.target + ' srcElement:' + msg.srcElement;
        }
    }
    msg = msg.toString();
	logger('ERROR', 'window.onerror event', url+':'+line_number+':'+column+': '+msg);
};

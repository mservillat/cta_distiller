(function($) {
	"use strict";

	var jobNames = ['ctbin'] //, 'astrocheck']
    var jobParameters = {
        evfile: "http://voplus.obspm.fr/cta/events.fits",
        emin: "0.1",
        emax: "100.0",
        enumbins: "20",
        nxpix: "200",
        nypix: "200",
        binsz: "0.02",
        coordsys: "CEL",
        xref: "85.25333404541016",
        yref: "22.01444435119629",
        proj: "TAN",
        "PHASE": "RUN",
};
        
    // LOAD JOB LIST AT STARTUP
    $(document).ready( function() {
    	
    	// Create UWS Client for ctbin
    	//uws_client.createClient(serviceUrl, jobName);
    	// get and show job list
    	//uws_client.getJobList();

    	// Create UWS Client
		var auth = $('#auth').attr('value');
    	uws_manager.initManager("https://voparis-uws-test.obspm.fr", jobNames, auth);
    	uws_manager.getJobList();
    	
    	// Refresh button
    	$('#get_jobs').click(  function() {
    		//uws_client.getJobList();
    		uws_manager.getJobList();
    	});
    	// Create Job button (for debug)
    	$('#create_job').click(  function() {
    		//uws_client.createTestJob(jobParameters);
    		uws_manager.createTestJob('ctbin', jobParameters);
    	});
    	if ( $( "#job_id" ).length ) {
    		//uws_client.selectJob($( "#job_id" ).attr('value'));
    		uws_manager.selectJob($( "#job_id" ).attr('value'));
    	}
    	    	
    });
    
})(jQuery);

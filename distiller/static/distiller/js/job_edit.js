(function($) {
	"use strict";

    var jobName;
    var jobId;

    $(document).ready( function() {
    	// If tab is defined in div tab, show the requested tab
		//var tab = $('#tab').attr('value');
    	//if (tab) {
    	//	$('#myTab a[href="#'+tab+'"]').tab('show');
    	//}
    	$('#form-buttons').remove();
    	// If jobId is defined in div job_id
    	jobId = $('#job_id').attr('value');
    	jobName = $('#job_name').attr('value');
    	if (jobName && jobId) {
//	    	// Create UWS Client for ctbin
//	    	uws_client.createClient(serviceUrl, jobName);
//	    	// Get and show job
//	    	uws_client.displaySingleJob(jobId);
			var auth = $('#auth').attr('value');
			uws_manager.initManager("https://voparis-uws-test.obspm.fr", [jobName], auth);
	    	uws_manager.displaySingleJob(jobName, jobId);
    	};
    });
    
//    // Use MutationObserver to know when the job row with jobId is displayed in the table #job_list
//	var targetNode = $("#job_list")[0];
//	var MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
//	var observer = new MutationObserver(mutationHandler);
//	var obsConfig = { childList: true, subtree: true };
//    observer.observe(targetNode, obsConfig);
//	// When Mutation occurs, check if it has id=jobId
//	function mutationHandler (mutationRecords) {
//	    mutationRecords.forEach ( function (mutation) {
//	    	//console.log(mutation.type);
//	        if ( mutation.type == "childList"
//	            &&  typeof mutation.addedNodes == "object"
//	            &&  mutation.addedNodes.length
//	        ) {
//		    	var myNodeList = mutation.addedNodes;
//		    	for (var i = 0; i < myNodeList.length; ++i) {
//		    		var node = myNodeList[i];  // Calling myNodeList.item(i) isn't necessary in JavaScript
//		    		//console.log(node.nodeClass);
//		    		if (node.nodeType === 1) {
//		    			if (node.nodeName == 'TR') {
//		    				console.log('Found!');
//		    				// jobId row has been created! (by uws_client)
//		    				if (typeof jobId === 'undefined') {
//    		    				jobId = node.getAttribute('id');
//    					    	// Send jobId to server session
//    		    				$.ajax({
//    								url: "/update_job_id",
//    								type: "GET",
//    								data: { job_id: jobId },
//    								dataType: "html",
//    								success : function(code_html, statut){
//    									console.log('job_id updated');
//    								},
//    								error : function(resultat, statut, erreur){
//    									console.log('ERROR in job_id update');
//    								}
//    							});
//    						};
////    						// Back to job list on remove
////	    				    $("#"+jobId).on("remove", function () {
////						    	window.location.href = "/job_list?job_deleted=true";
////						    });
////						    // Change click event for buttons
////	    				    //$("#"+jobId+" td button.parameters").attr("disabled", "disabled");
////	    				    $("#"+jobId+" td button.parameters").unbind('click');
////	    				    $('#'+jobId+' td button.parameters').click( function() {
////	    				    	$('#myTab a[href="#parameters"]').tab('show');
////	    				    });
////	    				    $("#"+jobId+" td button.properties").unbind('click');
////	    				    $('#'+jobId+' td button.properties').click( function() {
////	    				    	$('#myTab a[href="#properties"]').tab('show');
////	    				    });
////	    				    $("#"+jobId+" td button.results").unbind('click');
////	    				    $('#'+jobId+' td button.results').click( function() {
////	    				    	$('#myTab a[href="#results"]').tab('show');
////	    				    });
//	    				    // TODO: on phase change to COMPLETED, show results
//
//						    // Stop checking for mutations
//	    				    observer.disconnect();
//		    			}
//		    		}
//		    	}    
//		    } 
//	    });
//	}	 
    
})(jQuery);

from django.forms.util import ErrorList
import sys
import math
import xml.etree.ElementTree as ET
import StringIO
#import astropy
#import atpy
import numpy
#from astropy.table import Table
import urllib, urllib2
import logging

logger = logging.getLogger(__name__)

class DivErrorList(ErrorList):
    def __unicode__(self):
        return self.as_divs()
    def as_divs(self):
        if not self: return u''
        return u'%s' % '<br/>'.join([u'%s' % e for e in self])

def query2url(query, url, MAXREC=1000000, format='votable/td'):
    query_params = {
        'REQUEST':'doQuery', 
        'LANG': 'ADQL', 
        'QUERY': query}
    if MAXREC: query_params['MAXREC'] = str(MAXREC)
    if format: query_params['format'] = 'votable/td'
    query_params_enc = urllib.urlencode(query_params)
    ADQL_url = url + '/sync?' + query_params_enc
    return ADQL_url

def vo2json(votable):
    """
    Convert VO Table to json format for dataTables js lib
    Add a column with an index
    """
    from astropy.table import Table
    table = Table.read(StringIO.StringIO(votable), format='votable') #, pedantic=False, verbose=False)
    # Columns and filter properties
    columns = []
    filters = []
    # checkbox column
    columns.append({ 
        "field": "state", 
        'checkbox': True
    })
    # 1st column with id
    columns.append({ 
        "field": "id", 
        "title": "id", 
        "visible": False, 
        "type": 'search', 
        "sTitle": "id", 
        "sClass": "id", 
        "bVisible": False 
    })
    # Fill with VO Table columns keys
    for i,key in enumerate(table.columns.keys()):
        col = { 
            "field": key, 
            "title": key, 
            'valign': 'middle', 
            "sortable": True, 
            "switchable": False, 
            "sTitle": key 
        }
        values_raw = list( set( table[key].tolist() ) )
        values = []
        for v in values_raw:
            values.append({'id': str(v), 'label': str(v)})
        #print values
        filt = { 
            "field": key, 
            "label": key, 
            "type": 'select',
            "values": values
        }
        # Select minimum number of fields as visible
        if not key in ["dataproduct_type", "obs_collection", "obs_id", "target_name", "s_ra", "s_dec"]:
            col["visible"] = False
            col["switchable"] = True
            col["bVisible"] = False # for DataTables
        # If unit is defined, show it
        if table.columns[i].unit:
            col["title"] += " (" + str(table.columns[i].unit) + ")"
            col["sTitle"] += " (" + str(table.columns[i].unit) + ")" # for DataTables
        columns.append(col)
        filters.append(filt)
    # Fill with VO Table data
    data = []
    for i,row in enumerate(table):
        try:
            line = list( row )
            line.insert(0,False) # Add a checkbox to each row
            line.insert(1,i) # Add an index to each row
            line_dict = {}
            for j in range(len(line)):
                val = line[j]
                logger.debug(type(val))
                if isinstance(val, numpy.float32): val = round(val,8)
                if isinstance(val, numpy.int32): val = int(val)
                if isinstance(val, numpy.ma.core.MaskedConstant): val = ''
                line_dict[columns[j]['field']] = val
            data.append(line_dict)
        except Exception, e:
            raise e
    table_data = {
        "data" : data,
        "columns" : columns, 
        "filters" : filters
    }
    return table_data


def parse_sesame_result(s):
    """
    Extract ra, dec and names for the object if Sesame returned a result
    """
    # Read XML as etree
    root = ET.fromstring(s)
    try:
        # Get input name
        src_name = root.find(".//*name").text
        # Select first resolver (e.g. Simbad)
        resolver = root.find(".//*Resolver")
        # Get oname, otype, ra, dec
        oname = resolver.find("oname").text
        otype = resolver.find("otype").text
        ra = resolver.find("jradeg").text
        dec = resolver.find("jdedeg").text
        # Get aliases
        aliases = [elt.text for elt in resolver.findall("alias")]
        # Keep the aliases that match the input
        aliases_match = [alias for alias in aliases if src_name.lower() in alias.lower()]
        aliases_match.insert(0,oname)
        #print " ".join(aliases_match)
        return {'names': aliases_match, 'otype':otype, 'ra':ra, 'dec':dec, 'found':True}
    except:
        # The information on the source could not be found (no result or wrong XML)
        #print "Unexpected error:", sys.exc_info()[0]
        #raise
        return {'names': ['Not found in Simbad'], 'otype':'', 'ra':'', 'dec':'', 'found':False}
        #return None


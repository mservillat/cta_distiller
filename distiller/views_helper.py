from django.forms.utils import ErrorList
import forms
import sys, os
import logging
import math
import xml.etree.ElementTree as ET
import StringIO
#import atpy
import urllib, urllib2

logger = logging.getLogger(__name__)

class DivErrorList(ErrorList):
    def __unicode__(self):
        return self.as_divs()
    def as_divs(self):
        if not self: return u''
        return u'%s' % '<br/>'.join([u'%s' % e for e in self])

def query2url(query, url, MAXREC=1000000, format='votable/td'):
    query_params = {
        'REQUEST':'doQuery', 
        'LANG': 'ADQL', 
        'QUERY': query}
    if MAXREC: query_params['MAXREC'] = str(MAXREC)
    if format: query_params['format'] = 'votable/td'
    query_params_enc = urllib.urlencode(query_params)
    ADQL_url = url + '/sync?' + query_params_enc
    return ADQL_url

def vo2json(votable):
    """
    Convert VO Table to json format for dataTables js lib
    Add a column with an index
    """
    # Open with atpy
    # table = atpy.Table(StringIO.StringIO(votable), type='vo', pedantic=False, verbose=False)
    # Open with astropy.io.votable
    from astropy.io.votable import parse_single_table
    from numpy.ma.core import MaskedConstant
    table = parse_single_table(StringIO.StringIO(votable)).to_table()
    # Columns and filter properties
    columns = []
    filters = []
    # checkbox column
    columns.append({ 
        "field": "state", 
        'checkbox': True
    })
    # 1st column with id
    columns.append({ 
        "field": "id", 
        "title": "id", 
        "visible": False, 
        "type": 'search', 
        "sTitle": "id", 
        "sClass": "id", 
        "bVisible": False 
    })
    # Fill with VO Table columns keys
    for i,key in enumerate(table.columns.keys()):
        col = { 
            "field": key, 
            "title": key, 
            'valign': 'middle', 
            "sortable": True, 
            "switchable": False, 
            "sTitle": key 
        }
        #values_raw = list(set(table[key]))
        #values = []
        #for v in values_raw:
        #    if isinstance(v, MaskedConstant): v = ''
        #    if isinstance(v, float) and math.isnan(v): val = ''
        #    values.append({'id': str(v), 'label': str(v)})
        #print values
        filt = {
            "field": key, 
            "label": key, 
            "type": 'select',
            "values": []
        }
        # Select minimum number of fields as visible
        if not key in ["dataproduct_type", "obs_collection", "obs_id", "target_name", "s_ra", "s_dec"]:
            col["visible"] = False
            col["switchable"] = True
            col["bVisible"] = False # for DataTables
        # If unit is defined, show it
        cunit = table.columns.values()[i].unit
        if cunit:
            col["title"] += " (" + str(cunit) + ")"
            col["sTitle"] += " (" + str(cunit) + ")" # for DataTables
        columns.append(col)
        filters.append(filt)
    # Fill with VO Table data
    data = []
    for i in range(len(table)):
        row = table[i]
        try:
            line = list(row)
            line.insert(0,False) # Add an index to each row
            line.insert(1,i) # Add an index to each row
            line_dict = {}
            for j in range(len(line)):
                val = line[j]
                #if isinstance(val, numpy.float32): val = round(val,8)
                #if isinstance(val, numpy.int32): val = int(val)
                if isinstance(val, MaskedConstant): val = ''
                if isinstance(val, float) and math.isnan(val): val = ''
                line_dict[columns[j]['field']] = str(val)
            data.append(line_dict)
        except Exception, e:
            raise e
    table_data = {
        "data" : data,
        "columns" : columns, 
        "filters" : filters
    }
    #try:
    #    logger.info(data)
    #except Exception, e:
    #    raise e
    return table_data


def parse_sesame_result(s):
    """
    Extract ra, dec and names for the object if Sesame returned a result
    """
    # Read XML as etree
    root = ET.fromstring(s)
    try:
        # Get input name
        src_name = root.find(".//*name").text
        # Select first resolver (e.g. Simbad)
        resolver = root.find(".//*Resolver")
        # Get oname, otype, ra, dec
        oname = resolver.find("oname").text
        otype = resolver.find("otype").text
        ra = resolver.find("jradeg").text
        dec = resolver.find("jdedeg").text
        # Get aliases
        aliases = [elt.text for elt in resolver.findall("alias")]
        # Keep the aliases that match the input
        aliases_match = [alias for alias in aliases if src_name.lower() in alias.lower()]
        aliases_match.insert(0,oname)
        #print " ".join(aliases_match)
        return {'names': aliases_match, 'otype':otype, 'ra':ra, 'dec':dec, 'found':True}
    except:
        # The information on the source could not be found (no result or wrong XML)
        #print "Unexpected error:", sys.exc_info()[0]
        #raise
        return {'names': ['Not found in Simbad'], 'otype':'', 'ra':'', 'dec':'', 'found':False}
        #return None

def read_par(jobname):
    """
    Read .par file that contains the parameter list for a given tool
    The format of the file is from ftools/pfile, also used by ctools, it is an ASCII file with the following columns:
    name, type, parameter mode, default value, lower limit, upper limit, prompt
    """
    PROJECT_PATH = os.path.realpath(os.path.dirname(__file__))
    filename = PROJECT_PATH+'/par/'+jobname+'.par'
    from astropy.io import ascii
    data = ascii.read(filename, data_start=0, names=['name', 'type', 'mode', 'default', 'lower', 'upper', 'prompt'])
    import numpy as np
    return np.array(data)

def read_wadl(jobname, url='http://voparis-uws.obspm.fr/uws-v1.0/'):
    #from wadllib.application import Application
    import xml.etree.ElementTree as ET
    PROJECT_PATH = os.path.realpath(os.path.dirname(__file__))
    filename = PROJECT_PATH+'/wadl/'+jobname+'.wadl'
    f = open(filename,'r')
    wadl_string = f.read()
    f.close
    #wadl = Application(url, wadl_string)
    wadl = ET.fromstring(wadl_string)
    params_repr = wadl.find(".//{http://wadl.dev.java.net/2009/02}representation[@id='parameters']")
    params = []
    for p in params_repr.getchildren():
        param = {}
        param['name'] = p.get('name')
        param['type'] = p.get('type')
        param['required'] = p.get('required')
        param['default'] = p.get('default')
        param['prompt'] = p.getchildren()[0].text
        params.append(param)
    return params



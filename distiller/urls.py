from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls import patterns, include, url
#from django.views.generic import TemplateView
from django.views.generic import RedirectView
#from httpproxy.views import HttpProxy
#from distiller.views import UwsProxyView

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

# urlpatterns = patterns('',
#     # Access to media, ie images and js
#     url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
#     #url(r'^js_error_hook/*$', include('django_js_error_hook.urls')),
#     #url(r'^accounts/login/$', 'django.contrib.auth.views.login', {'template_name': 'login_django.html'}, name='login_view'),
#     #url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', name='logout_view'),
# )
urlpatterns = patterns('',
    # Home
    url(r'^home/*$', 'distiller.views.home', name='home'),
    # Logging
    url(r'^js_logger/*$', 'distiller.views.js_log', name='js_logger'),
    url(r'^show_logs/*$', 'distiller.views.show_logs', name='show_logs'),
    url(r'^show_info/*$', 'distiller.views.show_info', name='show_info'),
    url(r'^show_request/*$', 'distiller.views.show_request', name='show_request'),
    # Authentication
    url(r'^accounts/login/*$', 'distiller.views.login_view', name='login_view'),
    url(r'^accounts/logout/*$', 'distiller.views.logout_view', name='logout_view'),
    url(r'^accounts/create_user/*$', 'distiller.views.create_user', name='create_user'),
    # Browse all tables
    url(r'^browse/*$', 'distiller.views.search_form', name='browse_db'),
    # Access main page with search form (target name, RA, Dec...)
    url(r'^search/*$', 'distiller.views.search_form', name='search_form'),
    url(r'^results/*$', 'distiller.views.show_results', name='show_results'),
    # Proxy for Sesame name resolver
    url(r'^query_sesame/*$', 'distiller.views.query_sesame', name='query_sesame'),
    # Send query to TAP server
    url(r'^query_tapserver/*$', 'distiller.views.query_tapserver', name='query_tapserver'),
    # Send query to TAP server
    #url(r'^proxy/uws_server/(?P<job_name>\w*)/(?P<job_id>[-\w]+)/*$', 'distiller.views.uws_server', name='uws_server'),
    # Access a data file
    url(r'^get_data/*$', 'distiller.views.get_data', name='get_data'),
    # Manage jobs
    url(r'^job_list/*$', 'distiller.views.job_list', name='job_list'),
    #url(r'^job_edit/(?P<job_name>\w*)/(?P<job_id>[\w]{8}(-[\w]{4}){3}-[\w]{12})/*$', 'distiller.views.job_edit', name='job_edit'),
    url(r'^job_edit/(?P<job_name>\w*)/(?P<job_id>[-\w]+)/*$', 'distiller.views.job_edit', name='job_edit'),
    url(r'^job_form/(?P<job_name>\w*)/*$', 'distiller.views.job_form', name='job_form'),
    url(r'^job_send/(?P<job_name>\w*)/*$', 'distiller.views.job_send', name='job_send'),
    url(r'^update_job_id/*$', 'distiller.views.update_job_id', name='update_job_id'),
    # Visualisation
    url(r'^visu_js9/*$', 'distiller.views.visu_js9', name='visu_js9'),
                        
    # Examples:
    # url(r'^$', 'home', name='home'),
    # url(r'^app/', include('distiller.foo.urls')),
    # url(r'^articles/2003/$', views.special_case_2003),
    # url(r'^articles/(?P<year>[0-9]{4})/$', views.year_archive),
    # url(r'^articles/(?P<year>[0-9]{4})/(?P<month>[0-9]{2})/$', views.month_archive),
    # url(r'^articles/(?P<year>[0-9]{4})/(?P<month>[0-9]{2})/(?P<day>[0-9]{2})/$', views.article_detail),
 
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
 
    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    url(r'^$', RedirectView.as_view(url='/home')),
    #url(r'^/*$', RedirectView.as_view(url='/home'))
)

# Patterns for Authentication
urlpatterns += patterns('',
    url(r'^shib/', include('shibboleth.urls', namespace='shibboleth')),
    url(r'^social/', include('social.apps.django_app.urls', namespace='social')),
    url(r'^openid/', include('django_openid_auth.urls')),
    #url(r'^allauth/', include('allauth.urls')),
    url(r'^oidc/', include('djangooidc.urls', namespace='oidc')),
    #url(r'^oidc/', include('oidc_auth.urls')),
)

# urlpatterns += patterns('',
#     (r'^uws-v1.0/(?P<path>.*)$',
#         UwsProxyView.as_view()),
# )

# Patterns for static files
#urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
#urlpatterns += staticfiles_urlpatterns()

"""
WSGI config for cta_client project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/howto/deployment/wsgi/
"""

import os, sys

from django.core.wsgi import get_wsgi_application

sys.path.append('/share/web/cta-client')

# Needed for astropy (create astropy subdir and give write access to www)
envvars = {
    'XDG_CONFIG_HOME': '/share/web/cta-client/astropy/config',
    'XDG_CACHE_HOME': '/share/web/cta-client/astropy/cache'
}
os.environ.update(envvars)
os.environ['HTTPS'] = "on"

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "cta_client.settings")

application = get_wsgi_application()

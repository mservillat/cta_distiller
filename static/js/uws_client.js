// The client fills a Job List table with id=job_list
// <table id="job_list" class="table table-bordered table-striped table-condensed"></table>

// It can also show the job parameters located in form inputs with id=form_*

// The properties are listed in a table with id=prop_list
// <table id='prop_list' class="table table-bordered table-striped table-condensed"></table>

// The results are shown as bootstrap panels in a div block with id=result_list
// <div id='result_list' class='text-center'></div>

var uws_client = (function($) {
	"use strict";

	var jss = {};
    var client;
    var refreshPhaseInterval = {}; // stores setInterval functions for phase refresh
    var selectedJobId;
    var serviceUrl = "http://voparis-uws.obspm.fr/uws-v1.0/";
    var jobName = 'ctbin';

    // CREATE CLIENT
    function createClient(serviceUrl,jobName){
		this.serviceUrl = serviceUrl;
		this.jobName = jobName;
		client = new uwsLib.uwsClient(serviceUrl + jobName);
	};
    
    // SHOW INFO
    //var logger = glo

    // PREPARE TABLE
    var prepareTable = function() {
    	var tcontent = '\
			<thead>\
				<tr>\
					<th>Type</th>\
                    <th class="text-center">Start Time</th>\
                    <th class="text-center">Destruction Time</th>\
					<th class="text-center">Phase</th>\
                    <th class="text-center">Details</th>\
                    <th class="text-center">Control</th>\
				</tr>\
			</thead>\
			<tbody>\
			</tbody>';
    	$('#job_list').html(tcontent);
    };
    
    // SELECT JOB
    var selectJob = function(jobId) {
		$('#job_list tbody').find('tr.bg-info').removeClass('bg-info');
		$('#'+jobId).addClass("bg-info");
    }

    //----------
    // DISPLAY functions
    
    // DISPLAY PHASE
    var displayPhase = function(jobId, phase) {
    	var phase_class = 'btn-default';
    	var phase_icon = '';
    	clearInterval(refreshPhaseInterval[jobId]);
    	$('#'+jobId+' td button.results').attr("disabled", "disabled");
    	$('#'+jobId+' td button.start').attr("disabled", "disabled");
    	$('#'+jobId+' td button.abort').attr("disabled", "disabled");
    	switch (phase) {
    		case 'PENDING':
    			phase_class = 'btn-warning'; 
    			$('#'+jobId+' td button.start').removeAttr("disabled");
    			break;
    		case 'QUEUED': 
    			phase_class = 'btn-info'; 
    			$('#'+jobId+' td button.abort').removeAttr("disabled");
    			phase_icon = '<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>&nbsp;';
    			refreshPhaseInterval[jobId] = setInterval(getJobPhase, 10000, jobId);
    			break;
    		case 'EXECUTING': 
    			phase_class = 'btn-primary'; 
    			$('#'+jobId+' td button.abort').removeAttr("disabled");
    			phase_icon = '<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>&nbsp;';
    			refreshPhaseInterval[jobId] = setInterval(getJobPhase, 10000, jobId);
    			break;
    		case 'COMPLETED': 
    			phase_class = 'btn-success';
    			$('#'+jobId+' td button.results').removeAttr("disabled");
    			break;
    		case 'ERROR': 
    			phase_class = 'btn-danger'; 
    			break;
    		case 'ABORTED': 
    			phase_class = 'btn-danger'; 
    			break;
    		case 'UNKNOWN': 
    			phase_class = 'btn-danger'; 
    			$('#'+jobId+' td button.abort').removeAttr("disabled");
    			phase_icon = '<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>&nbsp;';
    			refreshPhaseInterval[jobId] = setInterval(getJobPhase, 10000, jobId);
    			break;
    		case 'HELD': 
    			phase_class = 'btn-default'; 
    			$('#'+jobId+' td button.abort').removeAttr("disabled");
    			$('#'+jobId+' td button.start').removeAttr("disabled");
    			break;
    		case 'SUSPENDED': 
    			phase_class = 'btn-default'; 
    			$('#'+jobId+' td button.abort').removeAttr("disabled");
    			refreshPhaseInterval[jobId] = setInterval(getJobPhase, 10000, jobId);
     			phase_icon = '<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>&nbsp;';
   			break;
    	};
    	$('#'+jobId+' td button.phase').html(phase_icon + phase);
    	$('#'+jobId+' td button.phase').removeClass( function (index, css) {
    	    return (css.match(/(^|\s)btn-\S+/g) || []).join(' ');
    	});
    	$('#'+jobId+' td button.phase').addClass(phase_class);
    };

    // REFRESH PHASE
    var refreshPhase = function(jobId, phase) {
    	var phase_init = $('#'+jobId+' td button.phase').html().split(";").pop();
    	logger('DEBUG',phase_init+' --> '+phase);
    	if (phase != phase_init) {
    		// Display PHASE button
    		displayPhase(jobId, phase);
	    	// Display properties in table prop_list
			if ($("#prop_list").length) {
				refreshProps(jobId);
			};
    		if (phase != 'PENDING') {
		    	// Set parameter inputs to readonly
				if ($("#job_params").length) {
        			$('[id^=id_]').attr('readonly','readonly');
        			$('[id^=button_]').attr('disabled','disabled');
				};
    		};
    		if (phase == 'COMPLETED') {
		    	// Display results as panels in div results
				if ($("#result_list").length) { refreshResults(jobId); }
				logger('INFO', 'Job completed '+jobId);
    		};
	    };
    };

    // DISPLAY JOB ROW
    var displayJobRow = function(job){
    	var start_time = job.startTime.split("T"); 
    	if (start_time.length == 1) {
    		start_time = "";
    	} else {
    	    start_time = start_time[0]+' '+start_time[1].split('+')[0];
    	};
    	var destr_time = job.destruction.split("T"); 
    	if (destr_time.length == 1) {
    		destr_time = "";
    	} else {
    	    destr_time = destr_time[0]+' '+destr_time[1].split('+')[0];
    	};
    	var row = '\
    		<tr id='+job.jobId+'>\
        		<td class="v-center">' + jobName + '</td>\
        		<td class="text-center v-center">' + start_time + '</td>\
        		<td class="text-center v-center">' + destr_time + '</td>\
        		<td class="text-center v-center">\
        			<button type="button" class="phase btn btn-default">PHASE...</button>\
        		</td>\
        		<td class="text-center">\
        			<div class="btn-group">\
        			<button type="button" class="parameters btn btn-default">\
            			<span class="glyphicon glyphicon-edit"></span>\
        				<span class="hidden-xs hidden-sm hidden-md">&nbsp;&nbsp;Parameters</span>\
        			</button>\
        			<button type="button" class="properties btn btn-default">\
        				<span class="glyphicon glyphicon-info-sign"></span>\
        				<span class="hidden-xs hidden-sm hidden-md">&nbsp;Properies</span>\
        			</button>\
        			<button type="button" class="results btn btn-default">\
            			<span class="glyphicon glyphicon-open"></span>\
        				<span class="hidden-xs hidden-sm hidden-md">&nbsp;Results</span>\
        			</button>\
        			</div>\
        		</td>\
        		<td class="text-center">\
        			<div class="btn-group">\
        			<button type="button" class="start btn btn-default">\
            			<span class="glyphicon glyphicon-play"></span>\
        				<span class="hidden-xs hidden-sm hidden-md">&nbsp;Start</span>\
        			</button>\
        			<button type="button" class="abort btn btn-default">\
            			<span class="glyphicon glyphicon-off"></span>\
        				<span class="hidden-xs hidden-sm hidden-md">&nbsp;Abort</span>\
        			</button>\
        			<button type="button" class="delete btn btn-default">\
            			<span class="glyphicon glyphicon-trash"></span>\
        				<span class="hidden-xs hidden-sm hidden-md">&nbsp;Delete</span>\
        			</button>\
        			</div>\
        		</td>\
    		</tr>';
    	// Insert row in table
    	$('#job_list tbody').prepend(row);
    	// Display phase according to phase status, and update on click
    	displayPhase(job.jobId, job.phase);
    	// Refresh phase button
    	$('#'+job.jobId+' td button.phase').click( function() {
    		var jobId = $(this).parents("tr").attr('id');
    		getJobPhase(jobId);
    	});
    	// Start job button
    	$('#'+job.jobId+' td button.start').click( function() {
    		var jobId = $(this).parents("tr").attr('id');
     		startJob(jobId);
    	});
    	// Abort job button
    	$('#'+job.jobId+' td button.abort').click( function() {
    		var jobId = $(this).parents("tr").attr('id');
    		var isOk = window.confirm("Abort job\nAre you sure?");
    		if (isOk) { 
    			abortJob(jobId);
    		};
    	});
    	// Delete job button
    	$('#'+job.jobId+' td button.delete').click( function() {
    		var jobId = $(this).parents("tr").attr('id');
    		var isOk = window.confirm("Delete job\nAre you sure?");
    		if (isOk) { 
    			destroyJob(jobId);
    		};
    	});
    };
    
    // DISPLAY JOB
    var displayJob = function(job){
    	// Display row
    	displayJobRow(job);
    	// Create Details buttons
    	$('#'+job.jobId+' td button.parameters').click( function() {
    		var jobId = $(this).parents("tr").attr('id');
    		selectJob(jobId);
    	    window.location.href = "/job_edit/"+jobId+'#parameters';
//    		var form = $('#job_edit');
//	        form.append('<input name="job_id" value="'+jobId+'" type="hidden"></input>');
//	        form.append('<input name="tab" value="parameters" type="hidden"></input>');
//	        form.submit();
    	});
    	$('#'+job.jobId+' td button.properties').click( function() {
    		var jobId = $(this).parents("tr").attr('id');
    		//getJobInfos(jobId);
    		selectJob(jobId);
    	    window.location.href = "/job_edit/"+jobId+'#properties';
//    		var form = $('#job_edit');
//	        form.append('<input name="job_id" value="'+jobId+'" type="hidden"></input>');
//	        form.append('<input name="tab" value="properties" type="hidden"></input>');
//	        form.submit();
    	});
     	$('#'+job.jobId+' td button.results').click( function() {
    		var jobId = $(this).parents("tr").attr('id');
    		//getJobResults(jobId);
    		selectJob(jobId);
    	    window.location.href = "/job_edit/"+jobId+'#results';
//    		var form = $('#job_edit');
//	        form.append('<input name="job_id" value="'+jobId+'" type="hidden"></input>');
//	        form.append('<input name="tab" value="results" type="hidden"></input>');
//	        form.submit();
    	});
    };

    // DISPLAY PARAMS
    var displayParams = function(job){
    	for (var p in job['parameters']) {
    		$('#id_'+p).attr('value', decodeURIComponent(job['parameters'][p]));
    		// Add update buttons (possible to update params when pĥase is PENDING in UWS 1.0 - but not yet implemented)
    		$('#id_'+p).wrap('<div class="input-group"></div>');
    		$('#id_'+p).parent().append('<span class="input-group-btn"><button id="button_'+p+'" class="btn btn-default" type="button">Update</button></span>');
    		if (job['phase'] != 'PENDING') {
    			$('#id_'+p).attr('readonly','readonly');
    			$('#button_'+p).attr('disabled','disabled');
    		}
    	};
    };
        
    // DISPLAY PROPS
    var displayProps = function(job){
    	for (var p in job) {
    		if (job[p]) {
	    		switch (p) {
	    			case 'parameters':
	    			case 'results':
	    				var res_str = '<tr><td><strong>'+p+'</strong></td><td>';
	    				for (var res in job[p]) {
	    					res_str += res+': '+decodeURIComponent(job[p][res])+'<br/>';
	    				};	
    					$('#prop_list').append(res_str+'</td></tr>');
    					break;
	    			default:
	    				$('#prop_list').append('<tr><td><strong>'+p+'</strong></td><td>'+job[p]+'</td></tr>');
	    		};
	    	};
    	};
    };
        
    // DISPLAY RESULTS
    var displayResults = function(job){
		$('#result_list').html('');
		var r_i = 0;
    	for (var r in job['results']) {
    		r_i++;
    		var r_id = 'result_'+r_i
    		var r_url = job['results'][r];
    		var r_name = r_url.split('/').pop();
    		var r_type = r_name.split('.').pop();
    		var r_panel = '\
				<div id="'+r_id+'" class="panel panel-default" value="'+r_url+'">\
				  	<div class="panel-heading clearfix">\
				  		<span class="pull-left">\
				  			<span class="panel-title"><strong>'+r_name+'</strong></span>: \
				  			<a href="'+r_url+'" target="_blank">'+r_url+'</a>\
				  		</span>\
				  	</div>\
				  	<div class="panel-body">\
				  		Preview\
				  	</div>\
				</div>';
    		$('#result_list').append(r_panel);
    		switch (r_type) {
	    		case 'fits':
	    			$('#'+r_id+' div.panel-heading span').attr('style', "padding-top: 5px;");
		    		$('#'+r_id+' div.panel-heading').append('\
		    			<button type="button" class="js9 btn btn-default btn-sm pull-right">JS9</button>\
		    			<button type="button" class="samp btn btn-default btn-sm pull-right">SAMP</button>\
		    		');
		    		// Add event on SAMP button click  
		    		$('#'+r_id+' div.panel-heading button.samp').click(function() {
		    			var url = $(this).parents(".panel").attr('value');
		    			var name = url.split('/').pop();
		    			samp_client.samp_image(url, name);
		    		});
		    		// Add event on JS9 button click  
		    		$('#'+r_id+' div.panel-heading button.js9').click(function() {
		    			var fits_url = $(this).parents(".panel").attr('value');
		    			//JS9.Load(url);
                		var form = $('#visu_js9');
            	        form.append('<input name="fits_url" value="'+fits_url+'" type="hidden"></input>');
            	        form.submit();
		    		});
		    		// Show image preview
		    		$('#'+r_id+' div.panel-body').html('\
		    			<img class="img-thumbnail" src="/static/images/crab_cta.png" />\
		    		');
		    		break;
		    	case 'log':
		    		// show textarea with log
		    		$('#'+r_id+' div.panel-body').html('\
	    				<textarea class="log form-control" rows="10" readonly></textarea>\
		    		');	    				
		    		$.ajax({
		    			url : r_url,
		    			dataType: "text",
		    			success : function (log) {
		    				$('#'+r_id+' div.panel-body textarea').html(log);
		    			}
		    		});
		    		break;
    		}
    	};
    };
    
    // DISPLAY SINGLE JOB INFO
    var displaySingleJob = function(jobId){
    	prepareTable();
    	client.getJobInfos(jobId, displaySingleJobSuccess, displaySingleJobError);
    };
    var displaySingleJobSuccess = function(job){
    	displayJobRow(job);
    	// Display parameters in id_* fields from form
    	displayParams(job);
    	// Display properties in table prop_list
    	displayProps(job);
    	// Display results as panels in div results
    	displayResults(job);
    	// Show div_job
    	$("#div_job").show();
    	// Slide to 
    	var hash = window.location.hash.substring(1);
    	scrollToAnchor(hash);
		// Back to job list on remove
	    $("#"+job.jobId).on("remove", function () {
	    	$("#div_job").hide();
	    	//$("#div_info").html('<strong>Job deleted</strong>: '+jobId+', going back to job list').addClass('alert alert-warning');
	    	//$("#div_info").html('<strong>Job deleted</strong>: '+jobId+', going back to job list').addClass('alert alert-success');
	    	window.location.href = "/job_list?job_deleted=true";
	    });
	    // Change click event for buttons
	    $('#'+job.jobId+' td button.parameters').click( function() {
	    	scrollToAnchor('parameters');
	    });
	    $('#'+job.jobId+' td button.properties').click( function() {
	    	scrollToAnchor('properties');
	    });
	    $('#'+job.jobId+' td button.results').click( function() {
	    	scrollToAnchor('results');
	    });
    };
    var displaySingleJobError = function(jobId, exception){
    	$("#div_job").hide();
    	$("#div_info").html('<strong>Job missing</strong>: '+jobId+', going back to job list').addClass('alert alert-warning');
    	logger('WARNING', 'displaySingleJob '+ jobId, exception);
    	window.location.href = "/job_list?job_missing=true";    	
    };

    // REFRESH PROPS
    var refreshProps = function(jobId){
    	client.getJobInfos(jobId, refreshPropsSuccess, refreshPropsError);
    };
    var refreshPropsSuccess = function(job){
		$("#prop_list").html('');
		displayProps(job);
		logger('DEBUG', 'Properties refreshed');
    };
    var refreshPropsError = function(jobId, exception){
    	logger('ERROR', 'refreshProps '+ jobId, exception);
    };

    // REFRESH RESULTS
    var refreshResults = function(jobId){
    	client.getJobInfos(jobId, refreshResultsSuccess, refreshResultsError);
    };
    var refreshResultsSuccess = function(job){
		displayResults(job);
		logger('DEBUG', 'Results refreshed');
    };
    var refreshResultsError = function(jobId, exception){
    	logger('ERROR', 'refreshResults '+ jobId, exception);
    };

    //----------
    // GET functions

    // GET JOB LIST
    var getJobList = function() {
    	//client.getJobs(getJobListSuccess,getJobListError);
    	client.getJobListInfos(getJobListSuccess,getJobListError)
    };
    var getJobListSuccess = function(jobs) {
		logger('INFO', 'Job list loaded');
    	if (jobs.length == 0){
    		$('#job_list').html(' ');
    		logger('INFO', 'Job list is empty');
    	}
    	else{
    		prepareTable();
    		for (var i = 0; i < jobs.length; i++) {
    			var job = jobs[i];
    			displayJob(job);
    		}
    	}
    };
    var getJobListError = function(exception) {
    	logger('ERROR', 'getJobList', exception);
    };

    // GET JOB PHASE
    var getJobPhase = function(jobId) {
    	client.getJobPhase(jobId, getJobPhaseSuccess, getJobPhaseError);
    };
    var getJobPhaseSuccess = function(jobId, phase) {
		logger('DEBUG', 'Phase is '+phase+' for job '+jobId);
    	refreshPhase(jobId, phase);
    };
    var getJobPhaseError = function(jobId, exception){
    	logger('ERROR', 'getJobPhase '+ jobId, exception);
    };
        
    // GET JOB INFO
    var getJobInfos = function(jobId){
    	client.getJobInfos(jobId,getJobInfosSuccess, getJobInfosError);
    };
    var getJobInfosSuccess = function(job){
    	alert(JSON.stringify(job, null, 4));
    };
    var getJobInfosError = function(jobId, exception){
    	logger('ERROR', 'getJobInfos '+ jobId, exception);
    };
    
    // GET JOB RESULTS
    var getJobResults = function(jobId){
    	client.getJobResults(jobId, getJobResultsSuccess, getJobResultsError);
    };
    var getJobResultsSuccess = function(jobId, results){
    	alert('Results for job '+ jobId + ' :\n' + JSON.stringify(results, null, 4));
    };
    var getJobResultsError = function(jobId, exception){
    	logger('ERROR', 'getJobResults '+ jobId, exception);
    };
    
    //----------
    // POST functions

    // CREATE JOB
    var createJob = function(jobParams) {
    	client.createJob(jobParams, createJobSuccess, createJobError);
    };
    var createJobSuccess = function(job) {
    	logger('INFO', 'Job created with id='+job.jobId);
    	// redirect to URL + job_id
		window.location.href = "/job_edit/"+job.jobId;
    };
    var createJobError = function(exception){
    	logger('ERROR', 'createJob', exception);
    };

    // CREATE TEST JOB
    var createTestJob = function(jobParams) {
    	client.createJob(jobParams, createTestJobSuccess, createTestJobError);
    };
    var createTestJobSuccess = function(job) {
    	logger('INFO', 'Test job created with id='+job.jobId);
    	displayJob(job);
    };
    var createTestJobError = function(exception){
    	logger('ERROR', 'createTestJob', exception);
    };

    // START JOB
    var startJob = function(jobId) {
    	client.startJob(jobId, startJobSuccess, startJobError);
    };
    var startJobSuccess = function(jobId) {
    	logger('INFO', 'Job started '+jobId);
    	getJobPhase(jobId);
    };
    var startJobError = function(jobId, exception){
    	logger('ERROR', 'startJob '+jobId, exception);
    };
    
    // ABORT JOB
    var abortJob = function (jobId){
    	client.abortJob(jobId, abortJobSuccess, abortJobError);
    };
    var abortJobSuccess = function(jobId){
    	logger('INFO', 'Job aborted '+jobId);
    	getJobPhase(jobId);
    };
    var abortJobError = function(jobId, exception){
    	logger('ERROR', 'abortJob '+ jobId, exception);
    };

    // DESTROY JOB
    var destroyJob = function (jobId){
    	client.destroyJob(jobId, destroyJobSuccess, destroyJobError);
    };
    var destroyJobSuccess = function(jobId, jobs){
        try {
            clearInterval(refreshPhaseInterval[jobId]);
            logger('INFO', 'Job deleted '+jobId);
            $('#'+jobId).remove();
        } catch (e) {
            logger('ERROR', 'destroyJobSuccess failed '+jobId, e);
        }
    };
    var destroyJobError = function(jobId, exception){
    	logger('ERROR', 'destroyJob '+jobId, exception);
    };
    
    //----------
	/* Exports. */
	jss.createClient = createClient;
	jss.prepareTable = prepareTable;
	jss.getJobList = getJobList;
	jss.displaySingleJob = displaySingleJob;
	jss.selectJob = selectJob;
	jss.createJob = createJob;
	jss.createTestJob = createTestJob;
	return jss;
    
})(jQuery);

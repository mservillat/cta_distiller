
var uwsClient = ( function() {
	"use strict";
	var SERVICE_URL;
	var init = function(serviceUrl){
		SERVICE_URL = serviceUrl;
	};

	var readResults = function(xml){
		var results = new Object();
		var resultsElement = $(xml).find("uws\\:results, results");
		$(resultsElement).find("uws\\:result, result").each(function(){
			var elementName = $(this).attr('id');
			var elementValue = $(this).attr("xlink:href");
			results[elementName] = elementValue ;
		});
		return results;
	};
	
	var getJobFromXml = function (xml){
		var jobXmlElement = $(xml).find("uws\\:job, job");
		var job = new Object();
		job.jobId = $(jobXmlElement).find("uws\\:jobId, jobId").text();
		job.phase = $(jobXmlElement).find("uws\\:phase, phase").text();
		job.startTime = $(jobXmlElement).find("uws\\:startTime, startTime").text();
		job.endTime = $(jobXmlElement).find("uws\\:endTime, endTime").text();
		job.executionDuration = $(jobXmlElement).find("uws\\:executionDuration, executionDuration").text();
		job.destruction = $(jobXmlElement).find("uws\\:destruction, destruction").text();
		job.ownerId = $(jobXmlElement).find("uws\\:ownerId, ownerId").text();
		job.quote = $(jobXmlElement).find("uws\\:quote, quote").text();
		job.parameters = new Object();
		var parametersElement = $(jobXmlElement).find("uws\\:parameters, parameters");
		$(parametersElement).find("uws\\:parameter, parameter").each(function(){
			var elementName = $(this).attr('id');
			var elementValue = $(this).text();
			job.parameters[elementName] = elementValue ;
		});
		job.results = readResults(xml);
		return job;
	};
	
	var getJobListFromXml = function (xml){
		var jobs = [];
		$(xml).find("uws\\:jobref, jobref").each(function(){
			var job = new Object();
			job.jobId = $(this).attr('id');
			job.phase = $(this).find('uws\\:phase, phase').text();
			jobs.push(job);
		});
		return jobs;
	};
	
	var getJobs = function(SuccessCallback, ErrorCallback){
		$.ajax({
			url : SERVICE_URL,
			async : true,
			type : 'GET',
			dataType: "xml",
			success : function(xml) {
				var jobs = getJobListFromXml(xml);
				SuccessCallback(jobs);
			},
			error : function(xhr, status, exception) {
				ErrorCallback(exception);
			}
		});
	};
	var createJob = function (jobParameters, SuccessCallback, ErrorCallback){
		$.ajax({
			url : SERVICE_URL,
			async : true,
			type : 'POST',
			data : jobParameters,
			success : function(xml) {
				var job = getJobFromXml(xml);
				SuccessCallback(job);
			},
			error : function(xhr, status, exception) {
				ErrorCallback(exception);
			}
		});
	};
	var destroyJob = function(id,successCallback, errorCallback) {
		$.ajax({
			url : SERVICE_URL + "/" + id,
			async : false,
			type: 'POST',
			dataType: "xml",
			data: "ACTION=DELETE",
			success : function(xml) {
				var jobs = getJobListFromXml(xml);
				successCallback(jobs);
			},
			error : function(xhr, status, exception) {
				errorCallback(id, exception);
			},
		});
		
	};
	
	var getJobInfos = function(id,successCallback, errorCallback) {
		$.ajax({
			url : SERVICE_URL + "/" + id,
			async : false,
			type: 'GET',
			dataType: "xml",
			success : function(xml) {
				var job = getJobFromXml(xml);
				successCallback(job);
			},
			error : function(xhr, status, exception) {
				errorCallback(id, exception);
			},
		});
	};
	
	var getJobResults = function(id,successCallback, errorCallback) {
		$.ajax({
			url : SERVICE_URL + "/" + id + "/results",
			async : false,
			type: 'GET',
			dataType: "xml",
			success : function(xml) {
				var results = readResults(xml);
				successCallback(id, results);
			},
			error : function(xhr, status, exception) {
				errorCallback(id, exception);
			},
		});
	};
	
	var getJobPhase = function(id,successCallback, errorCallback) {
		$.ajax({
			url : SERVICE_URL + "/" + id + "/phase",
			async : false,
			type: 'GET',
			success : function(xhr) {
				successCallback(id, xhr);
			},
			error : function(xhr, status, exception) {
				errorCallback(id, exception);
			},
		});
	};
	
	return {
		init: init,
		getJobs : getJobs,
		createJob : createJob,
		destroyJob : destroyJob,
		getJobInfos : getJobInfos,
		getJobResults : getJobResults,
		getJobPhase : getJobPhase
	}

})();




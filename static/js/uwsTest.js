(function($) {
	"use strict";
	var client;

var destroyJobSuccessCallback = function(jobs){
	jobsDisplaySuccessCallback(jobs);
};

var destroyJobErrorCallback = function(id, exception){
	$('#'+id+' > td.info').html('ERROR DESTROYING JOB '+exception);
};
var getJobInfosSuccessCallback = function(job){
// alert(job);
	alert(JSON.stringify(job, null, 4));
};
var getJobInfosErrorCallback = function(id, exception){
	alert("ERROR INFO JOB : " + exception);
};
var displayJobResultsSuccessCallback = function(id, results){
	alert('Results for job '+ id + ' :\n' + JSON.stringify(results, null, 4));
};
var displayJobResultsErrorCallback = function(id, exception){
	alert('Error getting results for job '+ id + ' :\n' + exception);
};
var displayJobPhaseSuccessCallback = function(id, phase){
	$('#'+id+' > td.phase').attr('value',phase);
};

var displayJobPhaseErrorCallback = function(id, exception){
	alert('Error getting phase for job '+ id + ' :\n' + exception);
};



var destroyJobOnClick = function (id){
	client.destroyJob(id,destroyJobSuccessCallback, destroyJobErrorCallback);
};
var displayJobInfos = function(id){
	client.getJobInfos(id,getJobInfosSuccessCallback, getJobInfosErrorCallback);
};
var displayJobResults = function(id){
	client.getJobResults(id,displayJobResultsSuccessCallback, displayJobResultsErrorCallback);
};
var displayJobPhase = function(id){
	client.getJobPhase(id,displayJobPhaseSuccessCallback, displayJobPhaseErrorCallback);
};

$(document).ready(function() {
	client = uwsClient;
	client.init(service_url);
	
	$('#get_jobs').click(function(){
		client.getJobs(jobsDisplaySuccessCallback,jobsDisplayErrorCallback);
	});
	$('#create_job').click(function(){
		client.createJob(jobParameters, displayJob, createJobDisplayErrorCallback);
	});
	
	$('#get_job_phase').click(function(){
		$.ajax({
			url : service_url +"/" + "12bc7378-21ce-a0e4-813f-698eda3e6f4f" + "/phase",
			async : true,
			type : 'GET',
			beforeSend : function(xhr) {
			},
			success : function(xhr, status) {
				$('#display_job_phase').html(xhr);
// alert(xhr);
			},
			error : function(xhr, status, exception) {
				alert('ERROR');
			}
		});
	});
});

var service_url = "http://voparis-uws.obspm.fr/uws-v1.0/ctbin";
var jobParameters = {evfile:"http://voplus.obspm.fr/cta/events.fits", enumbins:1, PHASE:"RUN"};


var jobsDisplaySuccessCallback = function(jobs){
	if (jobs.length == 0){
		$('#jobsTable tbody').html('NO JOB');
	}
	else{
		$('#jobsTable tbody').html("");
		for (var i = 0; i < jobs.length; i++) {
			var job = jobs[i];
			displayJob(job);
		}
	}
};
var jobsDisplayErrorCallback = function(exception){
	$('#jobsTable').html('ERROR GETTING JOB LIST : '+exception);
};

// var createJobDisplaySuccessCallback = function(job){
// $('#jobsTable tbody').append('<tr
// id='+job.jobId+'><td>'+job.jobId+'</td><td>'+job.phase+'</td>\
// <td><input class="destroy" type="button" value="Destroy Job"/></td>\
// <td><input class="details" type="button" value="Job Details"/></td></tr>');
// };
//
var createJobDisplayErrorCallback = function(exception){
	alert('ERROR CREATING JOB : '+exception);
};

var displayJob = function(job){
// $('#jobsTable tbody').append('<tr
// id='+job.jobId+'><td>'+job.jobId+'</td><td>'+job.phase+'</td>\
// <td><input class="destroy" type="button" value="Destroy Job"/></td>\
// <td><input class="details" type="button" value="Job Details"/></td>');
	var results;
	if (job.phase == 'COMPLETED'){
		results = '<td><input class="get_results" type="button" value="Get Results"/></td>';
	}
	else{
		results = '<td></td>';
	}
	$('#jobsTable tbody').append('<tr id='+job.jobId+'>\
			<td>'+job.jobId+'</td>\
			<td><input class="phase" type="button" value="'+job.phase+'"/></td>\
			<td><input class="destroy" type="button" value="Destroy Job"/></td>\
			<td><input class="details" type="button" value="Job Details"/></td>\
			'+results+'\
			</tr>');
	
	$('#jobsTable tbody').append('</tr>');
	
	$('#'+job.jobId+' > td > input.destroy').click(function(){
		var jobId = $(this).parents().eq(1).attr('id');
		destroyJobOnClick(jobId);
	});
	
	$('#'+job.jobId+' > td > input.details').click(function(){
		var jobId = $(this).parents().eq(1).attr('id');
		displayJobInfos(jobId);
	});
	
	$('#'+job.jobId+' > td > input.get_results').click(function(){
		var jobId = $(this).parents().eq(1).attr('id');
		displayJobResults(jobId);
	});
	$('#'+job.jobId+' > td > input.phase').click(function(){
		var jobId = $(this).parents().eq(1).attr('id');
		displayJobPhase(jobId);
	});
};
})(jQuery);
